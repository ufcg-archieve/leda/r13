# Roteiro 13 - AVL
### Árvore AVL

- [AVLTree](https://gitlab.com/ufcg-archieve/leda/r13/-/blob/078519655c68070853b389487a4193153975836c/src/main/java/adt/avltree/AVLTreeImpl.java)
- [AVLTreeVerifier](https://gitlab.com/ufcg-archieve/leda/r13/-/blob/078519655c68070853b389487a4193153975836c/src/main/java/adt/avltree/AVLTreeVerifierImpl.java)
- [AVLCountAndFill](https://gitlab.com/ufcg-archieve/leda/r13/-/blob/078519655c68070853b389487a4193153975836c/src/main/java/adt/avltree/AVLCountAndFillImpl.java)
