package adt.bst;

import java.util.ArrayList;
import java.util.List;

public class BSTImpl<T extends Comparable<T>> implements BST<T> {

	protected BSTNode<T> root;

	public BSTImpl() {
		root = new BSTNode<T>();
	}

	public BSTNode<T> getRoot() {
		return this.root;
	}

	@Override
	public boolean isEmpty() {
		return root.isEmpty();
	}

	@Override
	public int height() {
		return height(this.root);
	}

	protected int height(BSTNode<T> node) {
		int height = -1;
		if (!node.isEmpty()) {
			height = 1 + Math.max(height((BSTNode<T>) node.getLeft()), height((BSTNode<T>) node.getRight()));
		}
		return height;
	}

	@Override
	public BSTNode<T> search(T element) {
		return search(element, this.root);
	}

	private BSTNode<T> search(T element, BSTNode<T> node) {
		BSTNode<T> search = new BSTNode<>();
		if (element != null && !node.isEmpty()) {
			if (element.equals(node.getData())) {
				search = node;
			} else if (element.compareTo(node.getData()) < 0) {
				search = search(element, (BSTNode<T>) node.getLeft());
			} else {
				search = search(element, (BSTNode<T>) node.getRight());
			}
		}
		return search;
	}

	@Override
	public void insert(T element) {
		if (element != null && search(element).isEmpty()) {
			insert(element, this.root);
		}
	}

	private void insert(T element, BSTNode<T> node) {
		if (node.isEmpty()) {
			node.setData(element);
			node.setLeft(new BSTNode.Builder<T>().parent(node).build());
			node.setRight(new BSTNode.Builder<T>().parent(node).build());
		} else {
			if (element.compareTo(node.getData()) < 0) {
				insert(element, (BSTNode<T>) node.getLeft());
			} else {
				insert(element, (BSTNode<T>) node.getRight());
			}
		}
	}

	@Override
	public BSTNode<T> maximum() {
		BSTNode<T> max = null;
		if (!isEmpty()) {
			max = maximum(this.root);
		}
		return max;
	}

	private BSTNode<T> maximum(BSTNode<T> node) {
		BSTNode<T> max = node;
		if (!max.getRight().isEmpty()) {
			max = maximum((BSTNode<T>) node.getRight());
		}
		return max;
	}

	@Override
	public BSTNode<T> minimum() {
		BSTNode<T> min = null;
		if (!isEmpty()) {
			min = minimum(this.root);
		}
		return min;
	}

	private BSTNode<T> minimum(BSTNode<T> node) {
		BSTNode<T> min = node;
		if (!min.getLeft().isEmpty()) {
			min = minimum((BSTNode<T>) node.getLeft());
		}
		return min;
	}

	@Override
	public BSTNode<T> sucessor(T element) {
		BSTNode<T> node = search(element);
		BSTNode<T> sucessor = null;
		if (!node.isEmpty()) {
			if (!node.getRight().isEmpty()) {
				sucessor = minimum((BSTNode<T>) node.getRight());
			} else {
				sucessor = (BSTNode<T>) node.getParent();
				while (sucessor != null && !sucessor.isEmpty() && sucessor.getData().compareTo(element) < 0) {
					sucessor = (BSTNode<T>) sucessor.getParent();
				}
			}
		}
		return sucessor;
	}

	@Override
	public BSTNode<T> predecessor(T element) {
		BSTNode<T> node = search(element);
		BSTNode<T> predecessor = null;
		if (!node.isEmpty()) {
			if (!node.getLeft().isEmpty()) {
				predecessor = maximum((BSTNode<T>) node.getLeft());
			} else {
				predecessor = (BSTNode<T>) node.getParent();
				while (predecessor != null && !predecessor.isEmpty() && predecessor.getData().compareTo(element) > 0) {
					predecessor = (BSTNode<T>) predecessor.getParent();
				}
			}
		}
		return predecessor;
	}

	@Override
	public void remove(T element) {
		BSTNode<T> node = search(element);
		if (!node.isEmpty()) {
			
			// Nó folha (não possui filhos)
			if (node.isLeaf()) {
				node.setData(null);
			
			// Nó possui apenas 1 filho
			} else if (hasOnlyChild(node)) {
				BSTNode<T> childNode = node.getRight().isEmpty() ? (BSTNode<T>) node.getLeft() : (BSTNode<T>) node.getRight();
				if (this.root.equals(node)) {
					this.root = childNode;
					this.root.setParent(null);
				} else {
					childNode.setParent(node.getParent());
					if (node.getParent().getLeft().equals(node)) {
						node.getParent().setLeft(childNode);
					} else {
						node.getParent().setRight(childNode);
					}
				}
			
			// Nó possui 2 filhos
			} else {
				T sucessor = sucessor(node.getData()).getData();
				remove(sucessor);
				node.setData(sucessor);
			}
		}
	}

	protected boolean hasOnlyChild(BSTNode<T> node) {
		return (node.getLeft().isEmpty() && !node.getRight().isEmpty()) ||
				(!node.getLeft().isEmpty() && node.getRight().isEmpty());
	}

	@Override
	public T[] preOrder() {
		List<T> elements = new ArrayList<>();
		preOrder(elements, this.root);
		return (T[]) elements.toArray(new Comparable[elements.size()]);
	}

	private void preOrder(List<T> elements, BSTNode<T> node) {
		if (node != null && !node.isEmpty()) {
			elements.add(node.getData());
			preOrder(elements, (BSTNode<T>) node.getLeft());
			preOrder(elements, (BSTNode<T>) node.getRight());
		}
	}

	@Override
	public T[] order() {
		List<T> elements = new ArrayList<>();
		order(elements, this.root);
		return (T[]) elements.toArray(new Comparable[elements.size()]);
	}

	private void order(List<T> elements, BSTNode<T> node) {
		if (node != null && !node.isEmpty()) {
			order(elements, (BSTNode<T>) node.getLeft());
			elements.add(node.getData());
			order(elements, (BSTNode<T>) node.getRight());
		}
	}

	@Override
	public T[] postOrder() {
		List<T> elements = new ArrayList<>();
		postOrder(elements, this.root);
		return (T[]) elements.toArray(new Comparable[elements.size()]);
	}

	private void postOrder(List<T> elements, BSTNode<T> node) {
		if (node != null && !node.isEmpty()) {
			postOrder(elements, (BSTNode<T>) node.getLeft());
			postOrder(elements, (BSTNode<T>) node.getRight());
			elements.add(node.getData());
		}
	}

	/**
	 * This method is already implemented using recursion. You must understand
	 * how it work and use similar idea with the other methods.
	 */
	@Override
	public int size() {
		return size(root);
	}

	private int size(BSTNode<T> node) {
		int result = 0;
		// base case means doing nothing (return 0)
		if (!node.isEmpty()) { // indusctive case
			result = 1 + size((BSTNode<T>) node.getLeft())
					+ size((BSTNode<T>) node.getRight());
		}
		return result;
	}

}
