package adt.bst;

import adt.bt.BTNode;

/**
 * 
 * Performs consistency validations within a BST instance
 * 
 * @author Claudio Campelo
 *
 * @param <T>
 */
public class BSTVerifierImpl<T extends Comparable<T>> implements BSTVerifier<T> {
	
	private BSTImpl<T> bst;

	public BSTVerifierImpl(BST<T> bst) {
		this.bst = (BSTImpl<T>) bst;
	}
	
	private BSTImpl<T> getBSt() {
		return bst;
	}

	@Override
	public boolean isBST() {
		return bst.isEmpty() || isBST(bst.getRoot());
	}

	private boolean isBST(BTNode<T> node){
		boolean isBST = true;
		if (!node.isEmpty()) {
			if (isValidLeft(node) && isValidRight(node)) {
					isBST = isBST(node.getLeft()) && isBST(node.getRight());
			} else {
				 isBST = false;
			}
		}
		return isBST;
	}

	private boolean isValidLeft(BTNode<T> node) {
		return isValidLeft(node.getLeft(), node);
	}

	private boolean isValidLeft(BTNode<T> node, BTNode<T> root) {
		boolean isValid = true;
		if (!node.isEmpty()){
			if (node.getData().compareTo(root.getData()) < 0){
				isValid = isValidLeft(node.getLeft(), root) && isValidLeft(node.getRight(), root);
			} else {
				isValid = false;
			}
		}
		return isValid;
	}

	private boolean isValidRight(BTNode<T> node) {
		return isValidRight(node.getRight(), node);
	}

	private boolean isValidRight(BTNode<T> node, BTNode<T> root) {
		boolean isValid = true;
		if (!node.isEmpty()) {
			if (node.getData().compareTo(root.getData()) > 0) {
				isValid = isValidRight(node.getLeft(), root) && isValidRight(node.getRight(), root);
			} else {
				isValid = false;
			}
		}
		return isValid;
	}
	
}
